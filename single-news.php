<?php
/*
Template Name: News single template
Template Post Type: post
*/
get_header(); ?>
<main class="main" id="main">
	<?php  $the_id = ''; 
		if ( have_posts() ) : the_post(); $the_id = get_the_ID(); ?>
	<?php
	include(locate_template( 'inc/breadcrumbs.php'));

	$title = get_the_title();
	$titleClasses = 'page-title-sm page-title-bottom-md';
	include(locate_template( 'inc/title.php'));
	?>

	<section class="section blog-section margin-top-negative-md">
		<div class="wrap">
			<div class="main-image news-main-image">
				<?php the_post_thumbnail(); ?>
			</div>
			<div class="row-w content blog-content">
				<div class="col-10-w col-md-w">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>

	<section class="section blog-latest">
		<div class="wrap">
			<h2 class="t-center">
				ОСТАННІ НОВИНИ
			</h2>
			<div class="row news-amount">
				<?php
                    $arg = array(
                      	'post_status' => 'publish',
						'post_type' => 'post',
						'orderby' => 'publish',
                      	'posts_per_page' => 2,
                      	'post__not_in' => [ $the_id ]
                    ); 
                    $wp_query = new WP_Query( $arg );

                if ( $wp_query->have_posts() ) : 
					while ( $wp_query->have_posts() ) : $wp_query->the_post();
						$date = get_the_date( 'j.n.Y' , get_the_ID() );
				?>
				<div class="col-6 col-sm">
					<div class="card-news">
						<a href="#" class="card-img">
							<?php the_post_thumbnail(); ?>
						</a>
						<h2 class="card-title">
							<a href="#">
								<?php the_title(); ?>
							</a>
						</h2>
						<span class="card-date">
							<?php echo $date; ?>
						</span>
					</div>
				</div>
				<?php 
					endwhile; wp_reset_postdata(); 
        		endif; 
        		?>
			</div>
		</div>
	</section>
	<?php endif; ?>
</main>

<?php get_footer(); ?>