<?php

if ( ! function_exists( 'stem_setup' ) ) :

    function mavericksagnc_setup() {

         /*
             * Make theme available for translation.
             * Translations can be filed in the /languages/ directory.
             * If you're building a theme based on tecnavia, use a find and replace
             * to change 'tecnavia' to the name of your theme in all the template files.
             */
            load_theme_textdomain( 'mavericksagnc', get_template_directory() . '/languages' );

            // Add default posts and comments RSS feed links to head.
            add_theme_support( 'automatic-feed-links' );

            /*
             * Let WordPress manage the document title.
             * By adding theme support, we declare that this theme does not use a
             * hard-coded <title> tag in the document head, and expect WordPress to
             * provide it for us.
             */
            add_theme_support( 'title-tag' );

            /*
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support( 'post-thumbnails' );

            // This theme uses wp_nav_menu() in one location.
            register_nav_menus( array(
                'primary_navigation' => __('Primary Navigation', 'mavericksagnc'),
                'top_navigation' => __('Top Navigation', 'mavericksagnc')
            ) );

            /*
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support( 'html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            ) );

            // Add theme support for selective refresh for widgets.
            add_theme_support( 'customize-selective-refresh-widgets' );

    }
endif;
add_action( 'after_setup_theme', 'mavericksagnc_setup' );


function mavericksagnc_enqueue_scripts() {

    /*styles*/
    wp_enqueue_style( 'mavericksagnc_main_style', get_stylesheet_uri() );
    wp_enqueue_style( 'mavericksagnc_googleapis_style', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i' );
    wp_enqueue_style( 'mavericksagnc_normalize_style', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css' );
    wp_enqueue_style( 'mavericksagnc_slick-theme_style', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css' );
    wp_enqueue_style( 'mavericksagnc_slick_style', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css' );
    wp_enqueue_style( 'mavericksagnc_pikaday_style', 'https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css' );
    wp_enqueue_style( 'mavericksagnc_cssstyles_style', get_template_directory_uri().'/css/styles.css' );

    /*scripts*/
    wp_enqueue_script( 'mavericksagnc_jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', array('jquery'), '3.3.1', true );
    wp_enqueue_script( 'mavericksagnc_carousel', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array('jquery'), '1.9.0', true );
    wp_enqueue_script( 'mavericksagnc_pikaday', 'https://cdn.jsdelivr.net/npm/pikaday/pikaday.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'mavericksagnc_bundle_script', get_template_directory_uri() . '/assets/bundle.js', '', '1.0', true );

    wp_localize_script( 'mavericksagnc_bundle_script', 'mavericksagnc_ajax', 
        array(
            'url' => admin_url('admin-ajax.php')
        )
    );  

}
add_action( 'wp_enqueue_scripts', 'mavericksagnc_enqueue_scripts' );

## Отключить Гутенберг.
if( 'disable_gutenberg' ){
    remove_theme_support( 'core-block-patterns' );

    add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );

    remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );

    add_action( 'admin_init', function(){
        remove_action( 'admin_notices', [ 'WP_Privacy_Policy_Content', 'notice' ] );
        add_action( 'edit_form_after_title', [ 'WP_Privacy_Policy_Content', 'notice' ] );
    } );
}


require get_template_directory() . '/../mavericksagnc/inc/custom-post-type.php';
require get_template_directory() . '/../mavericksagnc/inc/custom-metaboxes.php';
require get_template_directory() . '/../mavericksagnc/inc/ajax.php';

add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){
    /*
    Вид базового шаблона:
    <nav class="navigation %1$s" role="navigation">
        <h2 class="screen-reader-text">%2$s</h2>
        <div class="nav-links">%3$s</div>
    </nav>
    */

    return '<div class="pagination">%3$s</div>';
}


define( 'THEME_IMG_PATH', get_stylesheet_directory_uri() );