<?php 
/*
Template Name: News-template
*/
get_header(); ?>
<main class="main" id="main">
	<?php
	include(locate_template( 'inc/breadcrumbs.php'));

	$titleClasses = 'page-title-bottom-md';
	$title = 'НОВИНИ';
	include(locate_template( 'inc/title.php'));
	/*--------------------------------------------------------------------------------*/
	global $wp_query;
                $paged = (get_query_var( 'paged' )) ? absint( get_query_var( 'paged' ) ) : 1;
                    $arg = array(
                      	'post_status' => 'publish',
						'post_type' => 'post',
						'orderby' => 'publish',
                      	'posts_per_page' => 3,
                      	'paged'          => $paged
                    ); 
                    $wp_query = new WP_Query( $arg );
	/*--------------------------------------------------------------------------------*/
	
	if ( $wp_query->have_posts() ) : 
	?>
	<section class="section margin-top-negative">
		<div class="wrap">
			<div class="row news-amount">
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); $date = get_the_date( 'j.n.Y' , get_the_ID() );?>
				<div class="col-6 col-sm">
					<div class="card-news">
						<a href="<?php echo get_permalink(); ?>" class="card-img">
							<img src="img/news.png" alt="" class="cover-img">
							<?php the_post_thumbnail(); ?>
						</a>
						<h2 class="card-title">
							<a href="<?php echo get_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
						<span class="card-date">
							<?php print( $date ) ?>
						</span>
					</div>
				</div>
			<?php endwhile; 

            wp_reset_postdata();
			?>

			</div>
			<?php the_posts_pagination(array(
				'end_size'     => 1,
			   'mid_size' => 1,
			   'prev_text' => 'Назад',
			   'next_text' => 'Далі',
			   'screen_reader_text' => ( '' )
			)); ?>
			<?php next_post_link($format, $link, true); ?>
		</div>
	</section>
	<?php endif; ?>
</main>

<?php get_footer(); ?>