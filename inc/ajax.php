<?php
add_action( 'wp_ajax_addposts_action', 'mavericksagnc_action_callback' );
add_action( 'wp_ajax_nopriv_addposts_action', 'mavericksagnc_action_callback' );

function mavericksagnc_action_callback() {
    $rtdata = array();
    $str = '';
    if ( isset( $_POST['count'] ) ) {

        $term = get_term_by('slug', 'sucсess-histories', 'event-type');

        $the_query = new WP_Query( array(
            'post_type'      => 'events',
            'post_status'    => 'publish',
            'posts_per_page' => 4,
            'offset'         => $_POST['count'],
            'order' => 'ASC',
            'tax_query' => array(
                                array(
                                'taxonomy' => 'event-type',
                                'field' => 'slug',
                                'terms' =>  array('sucсess-histories' )
                                 )
                              )
        ) );


        if($the_query->have_posts()) :
            
            while ( $the_query->have_posts() ) : $the_query->the_post();  
            $date = get_the_date( 'j.F.Y' , get_the_ID() );
            $date = explode('.', $date);
            $title= get_the_title();
            $str = $str .       
                    '<div class="col-6 col-md post-crd">
                        <div class="card-square">
                            <div class="card-square-inner">
                                <div class="card-square-date">
                                    <span class="day">'.
                                        $date[0] .
                                    '</span>
                                    <span class="month">' .
                                        $date[1] . ' ' . $date[2] .
                                    '</span>
                                </div>
                                <div class="card-square-title">
                                    <span># ' . $term->name . '</span>
                                    <h2>
                                        <a href="#" class="like-h3">'. 
                                            $title .
                                        '</a>
                                    </h2>
                                    <span>Андрій Вертихвіст</span>
                                </div>
                            </div>
                        </div>
                    </div>';

     
            endwhile; wp_reset_postdata(); 
        endif;

        $term = get_term( 4 );
        $rtdata[] = $str;
        $rtdata[] = $term->count;
    }
    echo json_encode($rtdata);
    wp_die();

}


add_action( 'wp_ajax_addnews_action', 'mavericksagnc_addnews_action_callback' );
add_action( 'wp_ajax_nopriv_addnews_action', 'mavericksagnc_addnews_action_callback' );

function mavericksagnc_addnews_action_callback() {
    $rtdata = array();
    $str = '';
    if ( isset( $_POST['paged'] ) ) {
        $date = get_the_date( 'j.F.Y' , get_the_ID() );

        $arg = array(
            'post_status' => 'publish',
            'post_type' => 'post',
            'orderby' => 'publish',
            'posts_per_page' => 3,
            'paged'          => $_POST['paged']
        ); 
        $wp_query = new WP_Query( $arg );
    
        if ( $wp_query->have_posts() ) : 

            while ( $wp_query->have_posts() ) : $wp_query->the_post(); $date = get_the_date( 'j.n.Y' , get_the_ID() );
                $str = $str .
                '<div class="col-6 col-sm">
                    <div class="card-news">
                        <a href="' . get_permalink() . '" class="card-img">
                            <img src="img/news.png" alt="" class="cover-img">' .
                            get_the_post_thumbnail() . 
                        '</a>
                        <h2 class="card-title">
                            <a href="<?php echo get_permalink(); ?>">' .
                                get_the_title() .
                            '</a>
                        </h2>
                        <span class="card-date">'
                            . $date .
                        '</span>
                    </div>
                </div>';
            endwhile; wp_reset_postdata(); 
        endif;

        $rtdata[] = $term->count;
    }
    echo $str;
    wp_die();

}


/*-------------------------------------------*/
add_action( 'wp_ajax_form_action', 'mavericksagnc_form_action_callback' );
add_action( 'wp_ajax_nopriv_form_action', 'mavericksagnc_form_action_callback' );

function mavericksagnc_form_action_callback() {
    $str = '';
    if ( isset( $_POST['formdataarr'] ) ) {
        $formdataarr = $_POST['formdataarr'];
        $postname =  json_decode( stripslashes($_POST['formdataarr']) );
        $post_data = array(
            'post_title'    => $postname->name,
            'post_type'=>'requests',
            'post_status'   => 'publish'
        );

        // Add post to the DB
        $post_id = wp_insert_post( $post_data );
        update_post_meta( $post_id, 'mavericksagnc-request', $formdataarr );
        $str = 'the post has been created';
    }
    echo $str;
   
    wp_die();

}