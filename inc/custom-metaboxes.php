<?php

add_action('add_meta_boxes', 'mavericksagnc_box');
function mavericksagnc_box(){
    add_meta_box( 'mavericksagnc-form', 'Заявка', 'mavericksagnc_request_box_field', 'requests', 'normal', 'low' );
}

/*
* Request metabox
*/

function mavericksagnc_request_box_field ($post) {
    wp_nonce_field( basename(__FILE__), 'mavericksagnc_request_mam_nonce' );
    $request = maybe_unserialize( get_post_meta( $post->ID, 'mavericksagnc-request', true ) );
    ?>
    <p><?php echo $request; ?></p>
    <?php
}

function mavericksagnc_request_save_data($post_id) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'mavericksagnc_request_mam_nonce' ] ) && wp_verify_nonce( $_POST[ 'mavericksagnc_request_mam_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

}

add_action('save_post',  'mavericksagnc_request_save_data' );