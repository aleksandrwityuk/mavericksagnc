<?php 

function register_mavericksagnc_post_type() {

	$slug = 'events';
	register_post_type($slug,
		array(
			'labels' => array(
				'name' => esc_html__( 'Events', 'mavericksagnc' ),
				'singular_name' => esc_html__( 'Events', 'mavericksagnc' )
			),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'capability_type' => 'post',
			'query_var' => true,
			'show_ui' => true,
			'show_in_menu' => true, 
			'show_in_admin_bar' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-calendar-alt',
			'menu_position' => 8,

			'rewrite' => array(
				'slug' => $slug,
			),
			'supports'           => array('title','editor','author','thumbnail','excerpt','comments'),
		)
	);
	$labels = array(
			'name'              => _x( 'Enent type', 'taxonomy general name', 'mavericksagnc' ),
			'singular_name'     => _x( 'Event type', 'taxonomy singular name', 'mavericksagnc' ),
			'search_items'      => __( 'Search Event type', 'mavericksagnc' ),
			'all_items'         => __( 'All Event types', 'mavericksagnc' ),
			'parent_item'       => __( 'Parent Event type', 'mavericksagnc' ),
			'parent_item_colon' => __( 'Parent Event type:', 'mavericksagnc' ),
			'edit_item'         => __( 'Edit Event type', 'mavericksagnc' ),
			'update_item'       => __( 'Update Event type', 'mavericksagnc' ),
			'add_new_item'      => __( 'Add New Event type', 'mavericksagnc' ),
			'new_item_name'     => __( 'New Event type Name', 'mavericksagnc' ),
			'menu_name'         => __( 'Event type', 'mavericksagnc' ),
	);
	
	$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'event-type' ),
	);
	
	register_taxonomy( 'event-type', array( $slug ), $args );

	$slug = 'requests';
	register_post_type($slug,
		array(
			'labels' => array(
				'name' => esc_html__( 'Requests', 'mavericksagnc' ),
				'singular_name' => esc_html__( 'Requests', 'mavericksagnc' )
			),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'capability_type' => 'post',
			'query_var' => true,
			'show_ui' => true,
			'show_in_menu' => true, 
			'show_in_admin_bar' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-calendar-alt',
			'menu_position' => 8,

			'rewrite' => array(
				'slug' => $slug,
			),
			'supports'           => array('title'),
		)
	);

}
add_action( 'init', 'register_mavericksagnc_post_type' );

?>