( function($) {

    var number = $( ".post-crd" ).length;

    $('#shezachod .btn').on("click", function (e){

        var data = {
            'action': 'addposts_action',
            'count' : number
        };
        jQuery.post( mavericksagnc_ajax.url, data, function( res ) {
            res = JSON.parse(res);
            if ( res[0].length > 14 ) {
                $('#pstcnt').append(res[0]);
            }
            number = $( ".post-crd" ).length;
            if ( res[1] == number ) {
                $('#shezachod').hide(); 
            }
            
        });

    });

    var body = $("html, body");
    $(".pagination .page-numbers").click(function (e){
        e.preventDefault();
        var val = $(this).text();
        if ( $(this).hasClass( "next" ) ) {
            val = $('.current').text();
            val++;
            $('.current').removeClass( "current" );
            $(':contains('+ val +')').addClass( 'current' );
        } else {
            $('.current').removeClass( "current" );
            $(this).addClass( 'current' );
        }
        

        body.stop().animate({scrollTop:200}, 1000, 'swing', function() { 
           
            var data = {
            'action': 'addnews_action',
            'paged' : val
            };
            jQuery.post( mavericksagnc_ajax.url, data, function( res ) {
                console.log(res);
                if ( res.length > 14 ) {
                    $('.news-amount').html(res);
                }
                
            });

        });

        
    });

    $('#test-mavericksagnc-form .btn').on("click", function (e){
        var name = $('#exampleInputName');
        var lastname = $('#exampleInputLastname');
        var post = $('#exampleInputPost');
        var website = $('#exampleInputWebSite');
        var coment = $('#exampleInputComents');
        console.log(coment);
        if ( name.val().length <= 2 ) {
            $('#InputName-valid').show();
            name.addClass('not-valid-form-item');
            e.preventDefault();
        } else if ( lastname.val().length <= 2 ) {
            $('#InputLastname-valid').show();
            lastname.addClass('not-valid-form-item');
            e.preventDefault();
        } else if ( coment.val().length <= 2 ) {
            $('#InputComents-valid').show();
            coment.addClass('not-valid-form-item');
            e.preventDefault();
        } else {
            var formdataarr = {
                'name' : name.val(),
                'lastname' : lastname.val(),
                'post' : post.val(),
                'website' : website.val(),
                'coment' : coment.val()
            }
            formdataarr = JSON.stringify(formdataarr);
            var data = {
                'action': 'form_action',
                'formdataarr' : formdataarr
            };
            jQuery.post( mavericksagnc_ajax.url, data, function( res ) {
                if ( res.length > 14 ) {
                    $('#popup-mavericksagnc-form').show();
                }
                console.log(res);
            });
        }

    });

} )(jQuery);